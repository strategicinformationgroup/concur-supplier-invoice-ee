/*xxcncrsi1.i - Concur integration - Supplier Inv QXI - TT definition*/
/* v0 - 2017-02-01 - SIG(jjh) */

{proxy/bcinvoicejournalentry/apimaintainbydatasetdef.i}

{proxy/bcdocument/apimaintainbydatasetdef.i}
{proxy/bcinvoice/apicreatecreditorinvoicesdef.i}

/*data structures  used for the Payment*/
define temp-table ttd_det no-undo
   field ttd_line as integer
   field ttd_desc1 as character
   field ttd_dr_amt as decimal
   field ttd_cr_amt as decimal
   field ttd_curr as character
   field ttd_acct as character
/* field ttd_sub as character       2018-06-11 */
   field ttd_proj as character   /* 2018-06-11 */
   field ttd_cc as character
   field ttd_saf_desc as character
   field ttd_saf_seq as integer
   field ttd_saf_par as character
   field ttd_saf_code as character
   field ttd_saf_concept as character
   field ttd_saf_struct as character
   index ttd_idx1 is primary ttd_line.

define temp-table tContextInfo no-undo
  field tcAction as character
  field tcActivityCode as character
  field tlPartialUpdate as logical
.

define temp-table tPosting no-undo
   field PostingDate as date
   field PostingText as character
   field PostingSecondText as character
   field tcJournalCode as character
   field tc_Rowid as character
   field tc_ParentRowid as character
   field CustomLong0 as character
.

define temp-table tPostingLine no-undo
   field PostingLineSequence as integer
   field PostingLineText as character
   field PostingLineSafText as character /*NOT SURE IF NEEDED - ALSO SEE BELOW*/
   field PostingLineDebitTC as decimal
   field PostingLineCreditTC as decimal
   field tcCurrencyCode as character
   field tcGLCode as character
/* field tcDivisionCode as character          2018-06-11 */
   field tcProjectCode as character        /* 2018-06-11 */
   field tcCostCentreCode as character
   field tc_Rowid as character
   field tc_ParentRowid as character
.

/*NOT SURE IF SAF IS USED BY QAD - WAS IN ORIG XLS AND XLS MACRO CODE*/
define temp-table tPostingSaf no-undo
   field PostingSafInputSequence as integer
   field PostingSafParentType as character
   field tcSafCode as character
   field tcSafConceptCode as character
   field tcSafStructureCode as character
   field tc_Rowid as character
   field tc_ParentRowid as character
.
/**************************************************************
define dataset BJournalEntry for
   tContextInfo,
   tPosting,
   tPostingLine,
   tPostingSaf
   data-relation PostingToPostingLine
      for tPosting, tPostingLine
      relation-fields(tPosting.tc_Rowid,
                      tPostingLine.tc_ParentRowid)
   data-relation PostingLineToPostingSaf
      for tPostingLine, tPostingSaf
      relation-fields(tPostingLine.tc_Rowid,
                      tPostingSaf.tc_ParentRowid)
.

define variable hbJournalEntry as handle.
assign hbJournalEntry = dataset BJournalEntry:handle.
*********************************************************/



/**************************************************************
define temp-table tApiCInvoiceCIJE no-undo
  field CInvoiceType as character
  field CInvoiceVoucher as integer
  field CInvoiceDate as character
  field CInvoicePostingDate as character
  field CInvoiceReference as character
  field CInvoiceDescription as character
  field CInvoiceOriginalDebitTC as decimal
  field CInvoiceOriginalCreditTC as decimal
  field CInvoiceAllocationStatus as character
  field CInvoiceIsInvoiceApproved as logical
  field CInvoiceIsTaxable as logical
  field CInvoicePoNbr as character
  field CInvoiceDayBookSetCode as character
  field CInvoiceSiteCode as character
  field tcReasonAllocationStatus as character
  field CInvoiceCommentNote as character
  field tiCAVoucher as decimal
  field tcCreditorCode as character
  field tcDivisionCode as character
  field tcJournalCode as character
  field tcCurrencyCode as character
  field tcReasonCode as character
  field tcPurchaseTypeCode as character
  field CInvoiceIsInitialStatus as logical
  field CustomShort0 as character
  field CustomShort6  as character
  field CustomLong0  as character
  field tc_Rowid as character
  field tc_ParentRowid as character
.

define temp-table tApiCInvoicePostingCIJE no-undo
  field CInvoicePostingType as character
  field tc_Rowid as character
  field tc_ParentRowid as character
.

define temp-table tApiCInvoiceVatCIJE no-undo
  field CInvoiceVatIsTaxable as logical
  field CInvoiceVatNonRecTaxAmtTC   as decimal
  field CInvoiceVatIsUpdAllow as logical
  field CInvoiceVatIsAbsRet as logical
  field CInvoiceVatIsAccrRcpUs as logical
  field CInvoiceVatTaxTrType as character
  field CInvoiceVatIsRevCharge as logical
  field CInvoiceVatUILinkedRowID as decimal
  field CInvoiceVatSequence as integer
  field TxtyTaxType as character
  field tlIsRecalculateTax as logical
  field tcDomainCode as character
  field tcVatInOut as character
  field tcVatCode as character
  field tc_Rowid as character
  field tc_ParentRowid as character
.

define temp-table tApiPostingCIJE no-undo
  field PostingVoucher as integer
  field PostingOriginIsExternal as logical
  field PostingIsReplacement as logical
  field PostingIsReversing as logical
  field PostingIsReversingBySign as logical
  field PostingIsZeroValueAllowed as logical
  field PostingIsSkipAutoAssignLC as logical
  field PostingVerifyStatus as character
  field PostingApproveStatus as character
  field PostingIsAutoReversal as logical
  field PostingAutoReversalType as character
  field PostingText as character
  field PostingDate as character
  field tiOriginVoucher as integer
/*  field tiOriginPostingId as integer   *removed from file, remove from here?*/
  field tcJournalCode as character
  field tcReportingJournalCode as character
  field tlJournalAccessAllowed as character
  field CustomShort0 as character
  field tc_Rowid as character
  field tc_ParentRowid as character
.

define temp-table tApiPostingLineCIJE no-undo
  field PostingLineSequence as integer
  field PostingLineDebitTC as decimal
  field PostingLineCreditTC as decimal
  field PostingLineText as character
  field PostingDate as character
  field tcGLCode as character
  field tcCurrencyCode as character
  field tcDivisionCode as character
  field tcCostCentreCode as character
  field CustomShort0 as character
  field tc_Rowid as character
  field tc_ParentRowid as character
.

define temp-table tApiPostingSafCIJE no-undo
  field PostingSafParentType as character
  field PostingSafInputSequence as integer
  field tcSafCode as character
  field tcSafConceptCode as character
  field tcSafStructureCode as character
  field tc_Rowid as character
  field tc_ParentRowid as character
.
  
*********************************************************/
define temp-table ttCommentNote no-undo
  field ttCommentLine as integer
  field ttCommentComment as character
.


define temp-table tApiCDocument
       field CDocument_ID as int64
       field Company_ID as int64
       field CCollection_ID as int64
       field Creditor_ID as int64
       field Currency_ID as int64
       field GL_ID as int64
       field PaySel_ID as int64
       field CreationUsr_ID as int64
       field BankNumber_ID as int64
       field CDocumentType as character
       field CDocumentSubType as character
       field CDocumentYear as integer
       field CDocumentNumber as integer
       field CDocumentReference as character
       field CDocumentOriginalCreditTC as decimal decimals 4
       field CDocumentOriginalCreditLC as decimal decimals 4
       field CDocumentOriginalCreditCC as decimal decimals 4
       field CDocumentOriginalDebitTC as decimal decimals 4
       field CDocumentOriginalDebitLC as decimal decimals 4
       field CDocumentOriginalDebitCC as decimal decimals 4
       field CDocumentIsOpen as logical
       field CDocumentDueDate as date
       field CDocumentStatus as character
       field CDocumentClosingDate as date
       field CDocumentValueDays as integer init "0"
       field CDocumentCreationDate as date
       field CDocumentTimesPrinted as integer init "0"
       field CDocumentLastPrintDate as date
       field CDocumentOriginalDebitBC as decimal decimals 4
       field CDocumentOriginalCreditBC as decimal decimals 4
       field CDocumentCreationTime as integer
       field CDocumentExchangeRate as decimal decimals 10
       field CDocumentRateScale as decimal decimals 10
       field CDocumentCCRate as decimal decimals 10
       field CDocumentCCScale as decimal decimals 10
       field CDocumentPrePrintedNumber as integer
       field CDocumentBankImpRef as character
       field CDocumentTransTpStat as character
       field CDocumentBankChargeAmountTC as decimal decimals 4
       field CDocumentBankChargeAmountLC as decimal decimals 4
       field CDocumentBankChargeAmountCC as decimal decimals 4
       field CDocumentBankChargeTaxAmountTC as decimal decimals 4
       field CDocumentBankChargeTaxAmountLC as decimal decimals 4
       field CDocumentBankChargeTaxAmountCC as decimal decimals 4
       field CDocumentReportingDateIN as date
       field tcCreditorCode as character
       field tcPaySelCode as character
       field tcCurrencyCode as character
       field tcBusinessRelationCode as character
       field tcBusinessRelationName as character
       field tlInvoicesLinked as logical
       field tcGLCode as character
       field ttPostingDate as date
       field tcPayFormatType as character
       field tcPostingRowId as character
       field tcCreditorBankNumber as character
       field tcPayFormatTypeCode as character
       field tcOwnBankNumber as character
       field tcPostingText as character
       field tdNetPaymentAmount as decimal decimals 4
       field tiOwnBankNumberId as int64
       field tlBankChargeIsValid as logical init "no"
       field tcBankChargeType as character
       field tcBankChargeGLCode as character
       field tlBankChargeRateIsFound as logical
       field tlBankChargeIsTaxable as logical init "no"
       field tcCDocumentOriginalStatus as character init "?"
       field tlBankChargeIsTaxIncluded as logical init "no"
       field tdBankChargeOriginalAmount as decimal decimals 4
       field tlOwnBankIsBankChargeEnable as logical
       field tcBankChargeTaxParameters as character
       field ttBankChargePostingDate as date
       field tcExternalUniqueIdentifier as character
       field tcCompanyCode as character
       field tcPostingJournalCode as character
       field tiPostingJournalId as int64
       field CustomShort0 as character
       field CustomShort1 as character
       field CustomShort2 as character
       field CustomShort3 as character
       field CustomShort4 as character
       field CustomShort5 as character
       field CustomShort6 as character
       field CustomShort7 as character
       field CustomShort8 as character
       field CustomShort9 as character
       field CustomCombo0 as character
       field CustomCombo1 as character
       field CustomCombo2 as character
       field CustomCombo3 as character
       field CustomCombo4 as character
       field CustomCombo5 as character
       field CustomCombo6 as character
       field CustomCombo7 as character
       field CustomCombo8 as character
       field CustomCombo9 as character
       field CustomLong0 as character
       field CustomLong1 as character
       field CustomNote as character
       field CustomDate0 as date
       field CustomDate1 as date
       field CustomDate2 as date
       field CustomDate3 as date
       field CustomDate4 as date
       field CustomInteger0 as integer init ?
       field CustomInteger1 as integer init ?
       field CustomInteger2 as integer init ?
       field CustomInteger3 as integer init ?
       field CustomInteger4 as integer init ?
       field CustomDecimal0 as decimal decimals 4
       field CustomDecimal1 as decimal decimals 4
       field CustomDecimal2 as decimal decimals 4
       field CustomDecimal3 as decimal decimals 4
       field CustomDecimal4 as decimal decimals 4
       field LastModifiedDate as date
       field LastModifiedTime as integer
       field LastModifiedUser as character
       field QADC01 as character
       field QADC02 as character
       field QADT01 as date
       field QADD01 as decimal decimals 10
       field tc_Rowid as character
       field tc_ParentRowid as character
       field tc_Status as character
       index i_parent tc_ParentRowid
       index i_status tc_Status
       index prim is unique is primary tc_Rowid.












/* Build the qDoc */
/********************************************************
define dataset BCInvoiceJournalEntry for
   tContextInfo,
   tApiCInvoiceCIJE,
   tApiCInvoicePostingCIJE,
   tApiCInvoiceVatCIJE,
   tApiPostingCIJE,
   tApiPostingLineCIJE,
   tApiPostingSafCIJE
   data-relation CInvoiceToCInvoicePosting
      for tApiCInvoiceCIJE, tApiCInvoicePostingCIJE
      relation-fields (tApiCInvoiceCIJE.tc_Rowid,
                       tApiCInvoicePostingCIJE.tc_ParentRowid)
   data-relation CInvoiceToCInvoiceVat
      for tApiCInvoiceCIJE, tApiCInvoiceVatCIJE
      relation-fields (tAPiCInvoiceCIJE.tc_Rowid,
                       tApiCInvoiceVatCIJE.tc_ParentRowid)
   data-relation ApiPostingToApiPostingLine
      for tApiPostingCIJE, tApiPostingLineCIJE
      relation-fields (tApiPostingCIJE.tc_Rowid,
                       tApiPostingLineCIJE.tc_ParentRowid)
   data-relation ApiPostingLineToAPiPostingLineSAF
      for tApiPostingLineCIJE, tApiPostingSafCIJE
      relation-fields(tApiPostingLineCIJE.tc_Rowid,
                      tApiPostingSafCIJE.tc_ParentRowid)
.
*********************************************************/

define variable hbcInvoiceJournalEntry as handle no-undo.

assign hbcInvoiceJournalEntry = dataset BCInvoiceJournalEntry:handle.














